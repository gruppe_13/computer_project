INTRODUCTION

------------

This is our first project. 
The authors: Artin Ghalamkary, Karsten Bak Malle, Phillip Ravn Boe Jensen.

The purpose of this project is to develop and program our robot(Burgerbot). 
Main goal is to make the robot complete a maze as fast and clean as possible.




SIMULATION

----------

You run the simulation following this order:

    1. Clone the git repository.
    
    2. Open a terminal, go to catkin_ws using cd (cd catkin_ws).
    
    3. When you're in catkin_ws, use the command "catkin_make".
    
    4. Open a new terminal and run the "roscore" command.
    
    5. Go back to the original terminal and run "roslaunch myburger myburger.launch"
    
    6. Now your Gazebo should open up with our customized maze. Have fun! 


